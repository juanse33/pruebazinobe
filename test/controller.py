import pandas as pd
import random
import hashlib
import json
from time import time
from bussines.sql import Sql
from bussines.methods import Methods
from testPrueba import TestPrueba

def main():
    con = Sql.sql_connection()
    Sql.sql_table(con)
    
    df_vacio = pd.DataFrame(columns=['Region','City Name','Languaje','Time'])

    region=[]
    pais=[]
    aleatorio=[]
    inicial = 0
    final = 0
    ejecucion = 0
    counter = 0
    for i in Methods.consultar_region():
        if i['region'] not in region and i['region'] :
            inicial = time()
            region.append(i['region'])
            paisResult = Methods.consultar_pais(i['region'])['name']
            lenguajeResult = Methods.consultar_lenguaje(paisResult)
            h = hashlib.sha1(str(lenguajeResult).encode('utf-8'))
            final = time() 
            ejecucion = final-inicial
            df_vacio = df_vacio.append({'Region':str(i['region']),
                            'City Name':str(paisResult),
                            'Languaje':h.hexdigest(),
                            'Time':float(ejecucion)}, ignore_index=True )
            Sql.sql_insert(con,str(counter),i['region'],str(paisResult),str(h.hexdigest()),str(ejecucion))
            counter = counter+1
    print('\n')
    print(df_vacio)
    print('\n')
    total = df_vacio['Time'].sum()
    print("Tiempo total: "+str(total))
    prom = df_vacio['Time'].mean()
    print("Tiempo promedio: "+str(prom))
    maximo = df_vacio['Time'].max()
    print("Tiempo maximo: "+str(maximo))
    minimo = df_vacio['Time'].min()
    print("Tiempo minimo: "+str(minimo))
    print('\n')
    print("---------------- SQLite ----------------")
    print('\n')
    Sql.sql_fetch(con)
    print('\n')
    print("--------- JSON ---------")
    print('\n')
    json_final = df_vacio.to_json()
    with open('data.json', 'w') as file:
        json.dump(json_final, file)
    print(json_final)        


if __name__ == "__main__":
   main()
   TestPrueba.main()


