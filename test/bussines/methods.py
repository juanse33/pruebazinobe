import random
import requests
class Methods():
    def consultar_region():
        url = "https://restcountries-v1.p.rapidapi.com/all"
        headers = {
        'x-rapidapi-key': "62225f9d5emsh5aa57b85c95c7b6p153f67jsn04871e19f7fd",
        'x-rapidapi-host': "restcountries-v1.p.rapidapi.com"
        }
        response = requests.request("GET", url, headers=headers)
        json_region = response.json()
        return json_region


    def consultar_pais(region):
        url = "https://restcountries.eu/rest/v2/region/"+region+"?fields=name"
        responses = requests.get(url)
        json_pais = responses.json()
        aleatorioPais = random.choice(json_pais)
        return aleatorioPais
    
    def consultar_lenguaje(pais):
        url = "https://restcountries.eu/rest/v2/name/"+pais+"?fields=languages"
        responses = requests.get(url)
        json_lenguaje = responses.json()
        lenguaje = json_lenguaje[0]['languages'][0]['name']
        return lenguaje