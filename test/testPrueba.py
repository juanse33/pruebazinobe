import unittest
from bussines.sql import Sql
from bussines.methods import Methods
class TestPrueba(unittest.TestCase):

    def testConsultar_region(self):
        self.assertIsNotNone(Methods.consultar_region())

    def testConsultar_pais(self):
        region=[]
        for i in Methods.consultar_region():
            if i['region'] not in region and i['region'] :
                region.append(i['region'])
                self.assertIsNotNone(i['region'])
                self.assertIsNotNone(Methods.consultar_pais(i['region']))

    def testConsultar_lenguaje(self):
        region=[]
        for i in Methods.consultar_region():
            if i['region'] not in region and i['region'] :
                region.append(i['region'])
                self.assertIsNotNone(Methods.consultar_lenguaje(Methods.consultar_pais(i['region'])['name']))

    def testOpen(self):
        con = Sql.sql_connection()
        self.assertIsNotNone(con)

    def main():
        unittest.main()

