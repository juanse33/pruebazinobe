PRUEBA ZINOBE 

Desarrolle una aplicacion en python que genere la tabla anterior teniendo las siguientes consideraciones:

- De https://rapidapi.com/apilayernet/api/rest-countries-v1, obtenga todas las regiones existentes.
- De https://restcountries.eu/ Obtenga un pais por region apartir de la region optenida del punto 1.
- De https://restcountries.eu/ obtenga el nombre del idioma que habla el pais y encriptelo con SHA1
- En la columna Time ponga el tiempo que tardo en armar la fila (debe ser automatico)
- La tabla debe ser creada en un DataFrame con la libreria PANDAS
- Con funciones de la libreria pandas muestre el tiempo total, el tiempo promedio, el tiempo minimo y el maximo que tardo en procesar toda las filas de la tabla.
- Guarde el resultado en sqlite.
- Genere un Json de la tabla creada y guardelo como data.json
- La prueba debe ser entregada en un repositorio git.
